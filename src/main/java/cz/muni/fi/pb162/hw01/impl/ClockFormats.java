package cz.muni.fi.pb162.hw01.impl;

import cz.muni.fi.pb162.hw01.Clock;
import cz.muni.fi.pb162.hw01.impl.formatting.PatternClock;
import cz.muni.fi.pb162.hw01.impl.formatting.SpelledClock;

/**
 * Time formatting access. Time is always formatted as it would be displayed on a clock.
 * <br>
 * NOTE:    The purpose of this class is to have access to your formatting implementation
 *          without actually imposing any requirements on your code structure.
 *          Your solution should be clean and extendable.
 *          
 */
public final class ClockFormats {

    /**
     * <p>
     * Formats clock's time (with precision of seconds) as english spelled words.
     * This formatter ignores days and milliseconds (though feel free to include it).
     * </p>
     *
     * e.g. {@code 13h 9m 24s 124ms} will be formatted as {@code thirteen hours, nine minutes and twenty-four seconds}
     * e.g. {@code 5h 2m 554ms} will be formatted as {@code five hours a two minutes}
     * e.g. {@code 1h 1m 1s} will be formatted as {@code one hour, one minute and one second}
     * e.g. {@code -1h 1m 1s} will be formatted as {@code twenty-three hours, one minute and one second}
     *
     * @param clock clock to format
     * @return formatted time as string
     */
    public static String spelled(Clock clock) {
        SpelledClock spelledClock = new SpelledClock();
        return spelledClock.getString(clock);
    }

    /**
     * <p>
     * Formats clock's time according to given pattern
     * </p>
     * <p>
     * Pattern is a formatting string where the following letters have special meaning.
     *
     * <ul>
     *     <li><strong>D</strong> Number of the day - variable number of digits</li>
     *     <li><strong>H</strong> Hour in day (0-23) - two digits</li>
     *     <li><strong>m</strong> Minute in hour - two digits</li>
     *     <li><strong>s</strong> Second in minute - two digits</li>
     *     <li><strong>S</strong> Millisecond in second - variable number of digits</li>
     * </ul>
     *
     * All other alfa-numeric characters should be considered as reserved.
     * Mote: This is quite simplified variant of patterns used by
     * <a href="https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/text/SimpleDateFormat.html">SimpleDateFormat</a>
     * </p>
     *
     * <p>
     * Note:
     *      {@code D} is the number of the day, not number of days. I.e. 1 means today, -1 means yesterday
     *      {@code D} is also the only portion which can be negative.
     *</p>
     *
     * @param clock clock to format
     * @param pattern pattern used to format
     * @return formatted time as string
     */
    public static String patterned(Clock clock, String pattern) {
        PatternClock patternClock = new PatternClock(pattern);
        return patternClock.getString(clock);
    }

}
