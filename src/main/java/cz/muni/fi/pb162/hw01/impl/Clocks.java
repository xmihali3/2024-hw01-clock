package cz.muni.fi.pb162.hw01.impl;

import cz.muni.fi.pb162.hw01.Clock;
import cz.muni.fi.pb162.hw01.TimeUnit;

/**
 * Factory class responsible for instantiation of {@link Clock} implementation
 */
public final class Clocks {

    /**
     * Creates new clock at {@code 00:00:00.000} time
     *
     * @return new clock
     */
    public static Clock instance() {
        return new UnitClocks();
    }

    /**
     * Creates new clock with the same time as the {@code other} clock
     *
     * @param clock the clock to copy
     * @return new clock
     */
    public static Clock instance(Clock clock) {
        return new UnitClocks(0, 0, 0, clock.getAs(TimeUnit.MILLIS));
    }

    /**
     * <p>
     * Creates new clock with specified time
     * <br>
     * All values can be negative. You can imagine this as adding each value to {@code 00:00:00.000} time
     * <br>
     * Hint: How you store these in your {@link Clock} will determine the difficulty of your solution
     * </p>
     *
     * @param hours hours part of time
     * @param minutes minutes part of time
     * @param seconds seconds part of time
     * @param millis millis part of time
     * @return new clock
     */
    public static Clock instance(long hours, long minutes, long seconds, long millis) {
        return new UnitClocks(hours, minutes, seconds, millis);
    }

    /**
     * Creates clocks
     */
    private Clocks() {
        // intentionally private
    }
}
