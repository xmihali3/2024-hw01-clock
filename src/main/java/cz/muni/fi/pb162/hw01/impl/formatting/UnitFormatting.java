package cz.muni.fi.pb162.hw01.impl.formatting;

import cz.muni.fi.pb162.hw01.Clock;
import cz.muni.fi.pb162.hw01.TimeUnit;

/**
 * Class for formatting clock units
 *
 * @author Jakub Mihálik
 */
public class UnitFormatting {
    private long millis;
    private final long seconds;
    private final long minutes;
    private final long hours;
    private final long days;

    /**
     * Creates formatted clock units
     *
     * @param clock clock to format
     */
    public UnitFormatting(Clock clock) {
        millis = clock.getAs(TimeUnit.MILLIS);
        days = millis / unitToMillis(TimeUnit.DAYS) + (millis < 0 ? -1 : 1);
        millis %= unitToMillis(TimeUnit.DAYS);
        if (millis < 0) {
            millis += unitToMillis(TimeUnit.DAYS);
        }

        hours = millis / unitToMillis(TimeUnit.HOURS);
        millis %= unitToMillis(TimeUnit.HOURS);
        minutes = millis / unitToMillis(TimeUnit.MINUTES);
        millis %= unitToMillis(TimeUnit.MINUTES);
        seconds = millis / unitToMillis(TimeUnit.SECONDS);
        millis %= unitToMillis(TimeUnit.SECONDS);
    }

    /**
     * Converts unit to milliseconds
     *
     * @param unit time unit
     * @return how many milliseconds are in the unit
     */
    public static int unitToMillis(TimeUnit unit) {
        return switch (unit) {
            case DAYS -> 86400000;
            case HOURS -> 3600000;
            case MINUTES -> 60000;
            case SECONDS -> 1000;
            case MILLIS -> 1;
        };
    }

    public long getMillis() {
        return millis;
    }

    public long getSeconds() {
        return seconds;
    }

    public long getMinutes() {
        return minutes;
    }

    public long getHours() {
        return hours;
    }

    public long getDays() {
        return days;
    }
}
