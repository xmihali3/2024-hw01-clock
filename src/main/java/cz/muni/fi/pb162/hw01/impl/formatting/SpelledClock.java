package cz.muni.fi.pb162.hw01.impl.formatting;

import cz.muni.fi.pb162.hw01.Clock;

/**
 * Class representing spelled clocks
 *
 * @author Jakub Mihálik
 */
public final class SpelledClock implements StringFormats {

    private static final String[] FORMATS = { "one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
            "seventeen", "eighteen", "nineteen", "twenty", "thirty", "forty", "fifty" };

    @Override
    public String getString(Clock clock) {
        UnitFormatting result = new UnitFormatting(clock);
        return makeString(result.getHours(), result.getMinutes(), result.getSeconds());
    }

    /**
     * Formats clock's time from hours, minutes and seconds
     *
     * @param hours hours
     * @param minutes minutes
     * @param seconds seconds
     * @return formatted time as string
     */
    private static String makeString(long hours, long minutes, long seconds) {
        String result = "";
        if (hours != 0) {
            result += numToString(hours) + (hours == 1 ? " hour" : " hours");
            if (minutes != 0 || seconds != 0) {
                result += minutes != 0 && seconds != 0 ? ", " : " and ";
            }
        }

        if (minutes != 0) {
            result += numToString(minutes) + (minutes == 1 ? " minute" : " minutes");
            if (seconds != 0) {
                result += " and ";
            }
        }

        if (seconds != 0) {
            result += numToString(seconds) + (seconds == 1 ? " second" : " seconds");
        }
        return result;
    }

    /**
     * Creates words from numbers
     *
     * @param longNum number in range 1-59
     * @return chosen number in word form
     */
    private static String numToString(long longNum) {
        int num = (int) longNum;
        if (num < 21) {
            return FORMATS[num - 1];
        }

        String result = FORMATS[17 + num / 10];
        num %= 10;

        if (num != 0) {
            result += "-" + FORMATS[num - 1];
        }

        return result;
    }
}
