package cz.muni.fi.pb162.hw01.impl;

import cz.muni.fi.pb162.hw01.Clock;
import cz.muni.fi.pb162.hw01.TimeUnit;
import static cz.muni.fi.pb162.hw01.impl.formatting.UnitFormatting.unitToMillis;

/**
 * Class representing clocks storing time as one unit (milliseconds)
 *
 * @author Jakub Mihálik
 */
public final class UnitClocks implements Clock {

    private final long millis;

    /**
     * Creates clocks
     */
    public UnitClocks() {
        millis = 0;
    }

    /**
     * Creates unit clock
     *
     * @param hours hours part of time
     * @param minutes minutes part of time
     * @param seconds seconds part of time
     * @param millis millis part of time
     */
    public UnitClocks(long hours, long minutes, long seconds, long millis) {
        this.millis = hours * unitToMillis(TimeUnit.HOURS)
                + minutes * unitToMillis(TimeUnit.MINUTES)
                + seconds * unitToMillis(TimeUnit.SECONDS)
                + millis;
    }

    @Override
    public Clock plus(Clock other) {
        return new UnitClocks(0, 0, 0, getAs(TimeUnit.MILLIS) + other.getAs(TimeUnit.MILLIS));
    }

    @Override
    public Clock minus(Clock other) {
        return new UnitClocks(0, 0, 0, getAs(TimeUnit.MILLIS) - other.getAs(TimeUnit.MILLIS));
    }

    @Override
    public long getAs(TimeUnit unit) {
        switch (unit) {
            case DAYS -> {
                return millis / unitToMillis(TimeUnit.DAYS);
            }
            case HOURS -> {
                return millis / unitToMillis(TimeUnit.HOURS);
            }
            case MINUTES -> {
                return millis / unitToMillis(TimeUnit.MINUTES);
            }
            case SECONDS -> {
                return millis / unitToMillis(TimeUnit.SECONDS);
            }
            case MILLIS -> {
                return millis;
            }
            default -> {
                return 0;
            }
        }
    }
}
