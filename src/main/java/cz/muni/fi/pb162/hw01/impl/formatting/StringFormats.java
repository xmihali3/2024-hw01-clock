package cz.muni.fi.pb162.hw01.impl.formatting;

import cz.muni.fi.pb162.hw01.Clock;

/**
 * interface representing formatted clocks
 *
 * @author Jakub Mihálik
 */
public interface StringFormats {

    /**
     * formats clock
     *
     * @param clock clock to format
     * @return formatted clock in string
     */
    String getString(Clock clock);
}
