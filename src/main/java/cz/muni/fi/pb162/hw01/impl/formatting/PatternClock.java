package cz.muni.fi.pb162.hw01.impl.formatting;

import cz.muni.fi.pb162.hw01.Clock;

/**
 * Class representing formatted clocks with pattern
 *
 * @author Jakub Mihálik
 */
public class PatternClock implements StringFormats {

    private final String pattern;

    /**
     * Creates patter formatting for clocks
     *
     * @param pattern pattern used to format
     */
    public PatternClock(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public String getString(Clock clock) {
        UnitFormatting result = new UnitFormatting(clock);
        return getPatternString(result.getDays(), result.getHours(),
                                result.getMinutes(), result.getSeconds(), result.getMillis());
    }

    /**
     * Formats time from given values according to given pattern
     *
     * @param days days
     * @param hours hours
     * @param minutes minutes
     * @param seconds seconds
     * @param millis milliseconds
     * @return formatted time as string
     */
    private String getPatternString(long days, long hours,
                                    long minutes, long seconds, long millis) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < pattern.length(); i++) {
            switch (pattern.charAt(i)) {
                case 'D':
                    result.append(days);
                    break;
                case 'H':
                    result.append(hours < 10 ? "0" + hours : hours);
                    break;
                case 'm':
                    result.append(minutes < 10 ? "0" + minutes : minutes);
                    break;
                case 's':
                    result.append(seconds < 10 ? "0" + seconds : seconds);
                    break;
                case 'S':
                    result.append(millis);
                    break;
                default:
                    result.append(pattern.charAt(i));
            }
        }
        return result.toString();
    }
}
