package cz.muni.fi.pb162.hw01.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClockFormatPatternedTest extends AbstractClockTest {

    @Test
    public void shouldFormatClockOnly() {
        var clock = Clocks.instance(4, 0, 0, 0);

        Assertions.assertThat(ClockFormats.patterned(clock, "H:m:s.S")).isEqualTo("04:00:00.0");
    }

    @Test
    public void shouldFormatClock() {
        var clock = Clocks.instance(4, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "H:m:s.S")).isEqualTo("04:22:42.300");
    }

    @Test
    public void shouldFormatClockMissingHours() {
        var clock = Clocks.instance(4, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "m:s.S")).isEqualTo("22:42.300");
    }

    @Test
    public void shouldFormatClockMissingMinutes() {
        var clock = Clocks.instance(4, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "H:s.S")).isEqualTo("04:42.300");
    }

    @Test
    public void shouldFormatClockMissingSeconds() {
        var clock = Clocks.instance(4, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "H:m.S")).isEqualTo("04:22.300");
    }

    @Test
    public void shouldFormatClockMissingMilliseconds() {
        var clock = Clocks.instance(4, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "H:m:s")).isEqualTo("04:22:42");
    }

    @Test
    public void shouldFormatClockDifferentSeparators() {
        var clock = Clocks.instance(4, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "H-m.s:S")).isEqualTo("04-22.42:300");
    }



    @Test
    public void shouldFormatClockWithDaysToday() {
        var clock = Clocks.instance(23, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(1) 23:22:42.300");
    }

    @Test
    public void shouldFormatClockWithDaysTomorrow() {
        var clock = Clocks.instance(25, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(2) 01:22:42.300");
    }

    @Test
    public void shouldFormatClockYesterdayHours() {
        var clock = Clocks.instance(-1, 0, 0, 0);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(-1) 23:00:00.0");
    }

    @Test
    public void shouldFormatClockYesterdayMinutes() {
        var clock = Clocks.instance(-1, 22, 0, 0);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(-1) 23:22:00.0");
    }

    @Test
    public void shouldFormatClockYesterday() {
        var clock = Clocks.instance(-1, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(-1) 23:22:42.300");
    }

    @Test
    public void shouldFormatClockPast() {
        var clock = Clocks.instance(-25, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(-2) 23:22:42.300");
    }

    @Test
    public void shouldFormatClockPastSimple() {
        var clock = Clocks.instance(-25, 0, 0, 0);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(-2) 23:00:00.0");
    }

    @Test
    public void shouldFormatClockFurtherPast() {
        var clock = Clocks.instance(-49, 22, 42, 300);

        Assertions.assertThat(ClockFormats.patterned(clock, "(D) H:m:s.S")).isEqualTo("(-3) 23:22:42.300");
    }
}
