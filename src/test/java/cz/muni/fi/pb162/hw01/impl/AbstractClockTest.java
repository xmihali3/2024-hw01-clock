package cz.muni.fi.pb162.hw01.impl;

import cz.muni.fi.pb162.hw01.Clock;
import cz.muni.fi.pb162.hw01.TimeUnit;
import org.assertj.core.api.Assertions;

public class AbstractClockTest {
    protected void assertClock(Clock clock, long days, long hours, long minutes, long seconds, long millis) {
        Assertions.assertThat(clock.getAs(TimeUnit.HOURS)).isEqualTo(hours);
        Assertions.assertThat(clock.getAs(TimeUnit.MINUTES)).isEqualTo(minutes);
        Assertions.assertThat(clock.getAs(TimeUnit.SECONDS)).isEqualTo(seconds);
        Assertions.assertThat(clock.getAs(TimeUnit.MILLIS)).isEqualTo(millis);
        Assertions.assertThat(clock.getAs(TimeUnit.DAYS)).isEqualTo(days);
    }
}
